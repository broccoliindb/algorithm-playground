/**
침투
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
1 초	512 MB	2836	1179	890	44.279%
문제
인제대학교 생화학연구실에 재직중인 석교수는 전류가 침투(percolate) 할 수 있는 섬유 물질을 개발하고 있다. 이 섬유 물질은 2차원 M × N 격자로 표현될 수 있다. 편의상 2차원 격자의 위쪽을 바깥쪽(outer side), 아래쪽을 안쪽(inner side)라고 생각하기로 한다. 또한 각 격자는 검은색 아니면 흰색인데, 검은색은 전류를 차단하는 물질임을 뜻하고 흰색은 전류가 통할 수 있는 물질임을 뜻한다. 전류는 섬유 물질의 가장 바깥쪽 흰색 격자들에 공급되고, 이후에는 상하좌우로 인접한 흰색 격자들로 전달될 수 있다.

김 교수가 개발한 섬유 물질을 나타내는 정보가 2차원 격자 형태로 주어질 때, 바깥쪽에서 흘려 준 전류가 안쪽까지 침투될 수 있는지 아닌지를 판단하는 프로그램을 작성하시오.

	
(a) The current percolates.	(b) The current does not percolate.
예를 들어, Figure 1(a) 에서는 바깥쪽에서 공급된 전류가 안쪽까지 침투하지만, Figure 1(b)에서는 그렇지 못한다.

입력
첫째 줄에는 격자의 크기를 나타내는  M (2 ≤ M ≤ 1,000) 과 N (2 ≤ N ≤ 1,000) 이 주어진다. M줄에 걸쳐서, N개의 0 또는 1 이 공백 없이 주어진다. 0은 전류가 잘 통하는 흰색, 1은 전류가 통하지 않는 검은색 격자임을 뜻한다.

출력
바깥에서 흘려준 전류가 안쪽까지 잘 전달되면 YES를 출력한다.

그렇지 않으면 NO를 출력한다.
예제 입력 1 
5 6
010101
010000
011101
100011
001011
예제 출력 1 
NO
예제 입력 2 
8 8
11000111
01100000
00011001
11001000
10001001
10111100
01010000
10001111
예제 출력 2 
YES
*/

// const M = 5
// const N = 6
// const data = [
//   [0, 1, 0, 1, 0, 1],
//   [0, 1, 0, 0, 0, 0],
//   [0, 1, 1, 1, 0, 1],
//   [1, 0, 0, 0, 1, 1],
//   [0, 0, 1, 0, 1, 1]
// ]
// const M = 7
// const N = 6
// const data = [
//   [0, 1, 0, 1, 0, 1],
//   [0, 1, 0, 0, 0, 0],
//   [0, 1, 1, 1, 0, 1],
//   [0, 0, 0, 0, 1, 1],
//   [0, 0, 1, 0, 1, 1],
//   [0, 0, 1, 0, 1, 1],
//   [1, 1, 1, 1, 1, 1]
// ]
// const M = 8
// const N = 8
// const data = [
//   [1, 1, 0, 0, 0, 1, 1, 1],
//   [0, 1, 1, 0, 0, 0, 0, 0],
//   [0, 0, 0, 1, 1, 0, 0, 1],
//   [1, 1, 0, 0, 1, 0, 0, 0],
//   [1, 0, 0, 0, 1, 0, 0, 1],
//   [1, 0, 1, 1, 1, 1, 0, 0],
//   [0, 1, 0, 1, 0, 0, 0, 0],
//   [0, 0, 0, 0, 1, 0, 1, 1]
// ]

const input = require('fs').readFileSync('/dev/stdin')
const info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => n)
  )
const [M, N] = info[0].map((i) => +i)
const data = info.slice(1).map((i) =>
  i
    .toString()
    .split('')
    .map((n) => +n)
)

const dx = [0, 0, -1, 1]
const dy = [-1, 1, 0, 0]
let visitedMatrix = Array(M)
  .fill()
  .map((i) => Array(N).fill(false))
let result = []

const init = (start) => {
  const [y, x] = start
  if (data[y][x] === 0 && !visitedMatrix[y][x]) {
    const isDone = bfs([y, x])
    if (!isDone) {
      const ax = x + 1
      if (ax <= M) {
        init([y, ax])
      }
    }
  } else {
    const ax = x + 1
    if (ax <= M - 1) {
      init([y, ax])
    } else {
      console.log('NO')
    }
  }
}
const bfs = (start) => {
  let queue = []
  queue.push(start)
  let isDone = true
  while (queue.length) {
    const [y, x] = queue[0]
    result.push(queue.shift())
    visitedMatrix[y][x] = true
    for (let k = 0; k < 4; k++) {
      let nx = x + dx[k]
      let ny = y + dy[k]
      if (nx >= 0 && nx < N && ny >= 0 && ny < M) {
        if (!visitedMatrix[ny][nx] && data[ny][nx] === 0) {
          queue.push([ny, nx])
          visitedMatrix[ny][nx] = true
        }
        if (ny < M - 2) {
          isDone = false
        }
        if (ny === M - 1 && data[ny][nx] === 0) {
          isDone = true
          console.log('YES')
          return isDone
        }
      }
    }
  }
  return isDone
}
init([0, 0])
