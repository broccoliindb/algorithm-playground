/**
2
3
Fred Barney
Barney Betty
Betty Wilma
3
Fred Barney
Betty Wilma
Barney Betty
*/

// const data = [
//   [2],
//   [3],
//   ['Fred', 'Barney'],
//   ['Barney', 'Betty'],
//   ['Betty', 'Wilma'],
//   [3],
//   ['Fred', 'Barney'],
//   ['Betty', 'Wilma'],
//   ['Barney', 'Betty']
// ]

const input = require('fs').readFileSync('/dev/stdin')
const info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => n)
  )
const data = info

class disjoinSet {
  constructor() {
    this.setList = {}
  }

  makeSet(element) {
    const node = {
      self: element,
      parent: element,
      count: 1
    }
    this.setList[element] = node
  }

  union(node1, node2) {
    const root1 = this.find(node1)
    const root2 = this.find(node2)
    if (root1 !== root2) {
      if (root1.count > root2.count) {
        root2.parent = root1
        root1.count = root1.count + root2.count
        console.log(root1.count)
      } else {
        root1.parent = root2
        root2.count = root2.count + root1.count
        console.log(root2.count)
      }
    } else {
      console.log(root1.count)
    }
  }

  find(node) {
    if (node.self === node.parent) {
      return node
    } else {
      node.parent = this.find(node.parent)
      return node.parent
    }
  }
}

const main = () => {
  const parent = []
  const [TEST_CASE] = data[0]

  let start = 2
  for (let j = 0; j < TEST_CASE; j++) {
    let set = new disjoinSet()
    const { setList } = set
    let testSize = parseInt(data[1], 10)
    let end = start + testSize
    for (let i = start; i < end; i++) {
      const [a, b] = data[i]
      if (!setList[a]) {
        set.makeSet(a)
      }
      if (!setList[b]) {
        set.makeSet(b)
      }

      set.union(setList[a], setList[b])
    }
    testSize = end
    start = testSize + 1
    end = start + testSize
  }
}

main()
