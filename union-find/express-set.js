/**
집합의 표현 스페셜 저지분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
2 초	128 MB	33049	11139	6789	30.081%
문제
초기에 {0}, {1}, {2}, ... {n} 이 각각 n+1개의 집합을 이루고 있다. 여기에 합집합 연산과, 두 원소가 같은 집합에 포함되어 있는지를 확인하는 연산을 수행하려고 한다.

집합을 표현하는 프로그램을 작성하시오.

입력
첫째 줄에 n(1≤n≤1,000,000), m(1≤m≤100,000)이 주어진다. m은 입력으로 주어지는 연산의 개수이다. 다음 m개의 줄에는 각각의 연산이 주어진다. 합집합은 0 a b의 형태로 입력이 주어진다. 이는 a가 포함되어 있는 집합과, b가 포함되어 있는 집합을 합친다는 의미이다. 두 원소가 같은 집합에 포함되어 있는지를 확인하는 연산은 1 a b의 형태로 입력이 주어진다. 이는 a와 b가 같은 집합에 포함되어 있는지를 확인하는 연산이다. a와 b는 n 이하의 자연수 또는 0이며 같을 수도 있다.

출력
1로 시작하는 입력에 대해서 한 줄에 하나씩 YES/NO로 결과를 출력한다. (yes/no 를 출력해도 된다)

예제 입력 1 
7 22
0 1 3
1 1 7
0 7 6
1 7 1
0 3 7
0 4 2
0 1 1
1 1 1
1 1 2
1 1 3
1 1 4
1 1 5
1 1 6
1 1 7
0 5 4
1 1 1
1 1 2
1 1 3
1 1 4
1 1 5
1 1 6
1 1 7
예제 출력 1 
NO
NO
YES
*/
// const N = 7
// const M = 28
// const data = [
//   [0, 1, 3],
//   [1, 1, 7],
//   [0, 7, 6],
//   [1, 7, 1],
//   [0, 3, 7],
//   [0, 4, 2],
//   [0, 1, 1],
//   [1, 1, 1],
//   [1, 1, 2],
//   [1, 1, 3],
//   [1, 1, 4],
//   [1, 1, 5],
//   [1, 1, 6],
//   [1, 1, 7],
//   [0, 1, 1],
//   [0, 1, 2],
//   [0, 1, 3],
//   [0, 1, 4],
//   [0, 1, 5],
//   [0, 1, 6],
//   [0, 1, 7],
//   [1, 1, 1],
//   [1, 1, 2],
//   [1, 1, 3],
//   [1, 1, 4],
//   [1, 1, 5],
//   [1, 1, 6],
//   [1, 1, 7]
// ]

class DisjointSet {
  constructor(n) {
    this.setList = new Map()
    for (let i = 1; i <= n; i++) {
      this.setList.set(i, this.makeSet())
    }
  }

  makeSet() {
    const singleton = {
      rank: 0
    }
    singleton.parent = singleton
    return singleton
  }

  union(node1, node2) {
    const root1 = this.find(node1)
    const root2 = this.find(node2)
    if (root1 !== root2) {
      if (root1.rank < root2.rank) {
        root1.parent = root2
      } else {
        root2.parent = root1
        if (root1.rank === root2.rank) {
          root1.rank += 1
        }
      }
    }
  }

  find(node) {
    if (node === node.parent) {
      return node
    } else {
      node.parent = this.find(node.parent)
      return node.parent
    }
  }
}

const info = []
require('readline')
  .createInterface({
    input: process.stdin,
    output: process.stdout
  })
  .on('line', (line) => {
    info.push(line.split(/\s+/).map((n) => +n))
  })
  .on('close', () => {
    main()
    process.exit()
  })

const main = () => {
  const [N, M] = info[0]
  const data = info.slice(1)
  const disjointSet = new DisjointSet(N)
  for (let i = 0; i < M; i++) {
    const [op, a, b] = data[i]
    if (op === 0) {
      disjointSet.union(disjointSet.setList.get(a), disjointSet.setList.get(b))
    } else {
      const r1 = disjointSet.find(disjointSet.setList.get(a))
      const r2 = disjointSet.find(disjointSet.setList.get(b))
      if (r1 === r2) {
        console.log('YES')
      } else {
        console.log('NO')
      }
    }
  }
  console.log(disjointSet.setList)
}
// main()
