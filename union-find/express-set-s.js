// const input = require('fs').readFileSync('/dev/stdin')
// const info = input
//   .toString()
//   .trim()
//   .split(/\n+/)
//   .map((i) =>
//     i
//       .toString()
//       .trim()
//       .split(/\s+/)
//       .map((n) => +n)
//   )
// const [N, M] = info[0]
// const data = info.slice(1)

// const N = 10000
// const M = 20000
// const setData = () => {
//   let datas = []
//   for (let i = 0; i < M; i++) {
//     const op = Math.floor(Math.random() * 2)
//     const a = Math.floor(Math.random() * N + 1)
//     const b = Math.floor(Math.random() * N + 1)
//     datas.push([op, a, b])
//   }
//   return datas
// }
// const data = setData()

// const info = []
// const readline = require('readline')
// readline
//   .createInterface({
//     input: process.stdin,
//     output: process.stdout
//   })
//   .on('line', (line) => {
//     info.push(line.split(/\s+/).map((n) => +n))
//   })
//   .on('close', () => {
//     solve()
//     process.exit()
//   })

const n = 7
const m = 8
const data = [
  [0, 1, 3],
  [1, 1, 7],
  [0, 7, 6],
  [1, 7, 1],
  [0, 3, 7],
  [0, 4, 2],
  [0, 1, 1],
  [1, 1, 1]
]

class DisjointSet {
  constructor(n) {
    this.parent = Array(n + 1)
    for (let i = 0; i <= n; ++i) {
      this.parent[i] = i
    }
  }

  union(n1, n2) {
    const rootA = this.find(n1)
    const rootB = this.find(n2)
    this.parent[rootA] = rootB
  }

  find(node) {
    if (this.parent[node] === node) {
      return node
    }
    this.parent[node] = this.find(this.parent[node])
    return this.parent[node]
  }
}

function solve() {
  // const [n, m] = info[0]
  // const data = info.slice(1)
  const disjointSet = new DisjointSet(n)
  let answer = ''
  for (let i = 0; i < m; ++i) {
    const [op, a, b] = data[i]
    if (op === 0) {
      disjointSet.union(a, b)
    } else if (op === 1) {
      const rootA = disjointSet.find(a)
      const rootB = disjointSet.find(b)
      if (rootA === rootB) {
        answer += 'YES\n'
      } else {
        answer += 'NO\n'
      }
    }
  }
  console.log(answer)
}

solve()
