/**
여행 가자 분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
2 초	128 MB	10318	3938	2973	39.180%
문제
동혁이는 친구들과 함께 여행을 가려고 한다. 한국에는 도시가 N개 있고 임의의 두 도시 사이에 길이 있을 수도, 없을 수도 있다. 동혁이의 여행 일정이 주어졌을 때, 이 여행 경로가 가능한 것인지 알아보자. 물론 중간에 다른 도시를 경유해서 여행을 할 수도 있다. 예를 들어 도시가 5개 있고, A-B, B-C, A-D, B-D, E-A의 길이 있고, 동혁이의 여행 계획이 E C B C D 라면 E-A-B-C-B-C-B-D라는 여행경로를 통해 목적을 달성할 수 있다.

도시들의 개수와 도시들 간의 연결 여부가 주어져 있고, 동혁이의 여행 계획에 속한 도시들이 순서대로 주어졌을 때(중복 가능) 가능한지 여부를 판별하는 프로그램을 작성하시오.

입력
첫 줄에 도시의 수 N이 주어진다. N은 200이하이다. 둘째 줄에 여행 계획에 속한 도시들의 수 M이 주어진다. M은 1000이하이다. 다음 N * N 행렬을 통해 임의의 두 도시가 연결되었는지에 관한 정보가 주어진다. 1이면 연결된 것이고 0이면 연결이 되지 않은 것이다. A와 B가 연결되었으면 B와 A도 연결되어 있다. 마지막 줄에는 여행 계획이 주어진다.

출력
첫 줄에 가능하면 YES 불가능하면 NO를 출력한다.

예제 입력 1 
3
3
0 1 0
1 0 1
0 1 0
1 2 3
예제 출력 1 
YES
예제 입력 2
5
2
0 1 1 0 0
1 0 0 0 0
1 0 0 0 0
0 0 0 0 1
0 0 0 1 0
1 5
예제 출력 2
NO
*/
// const N = 3
// const M = 3
// const data = [
//   [0, 1, 0],
//   [1, 0, 1],
//   [0, 1, 0]
// ]
// const plan = [1, 2, 3]

// const N = 5
// const M = 2
// const data = [
//   [0, 1, 1, 0, 0],
//   [1, 0, 0, 0, 0],
//   [1, 0, 0, 0, 0],
//   [0, 0, 0, 0, 1],
//   [0, 0, 0, 1, 0]
// ]
// const plan = [1, 5]

let input = require('fs').readFileSync('/dev/stdin')
let info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => +n)
  )
const [N] = info[0]
const [M] = info[1]
const data = info.slice(2, info.length - 1)
const plan = info[info.length - 1]

class DisjointSet {
  constructor(n) {
    this.parent = []
    for (let i = 0; i < n; i++) {
      this.parent[i] = i
    }
  }

  find(node) {
    if (this.parent[node] !== node) {
      const root = this.parent[node]
      this.parent[node] = this.find(root)
      return this.parent[node]
    } else {
      return node
    }
  }

  union(node1, node2) {
    const root1 = this.find(node1)
    const root2 = this.find(node2)
    this.parent[root2] = root1
  }
}

const main = () => {
  const disjointSet = new DisjointSet(N)
  for (let j = 0; j < N; j++) {
    for (let i = 0; i < N; i++) {
      if (i !== j && data[i][j] === 1) {
        const root1 = disjointSet.find(i)
        const root2 = disjointSet.find(j)
        if (root1 !== root2) {
          disjointSet.union(i, j)
        }
      }
    }
  }
  // console.log(disjointSet.parent)

  const result = []
  let isPossible = true
  for (let i = 0; i < M; i++) {
    const city = plan[i] - 1
    const rootCity = disjointSet.find(city)
    if (!result.length) {
      result.push(rootCity)
    }
    if (result && result[0] !== rootCity) {
      isPossible = false
      break
    }
  }
  console.log(isPossible ? 'YES' : 'NO')
}

main()
