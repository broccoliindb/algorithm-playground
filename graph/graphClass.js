class Graph {
  constructor() {
    this.nodeList = new Map()
    this.adjacencyList = new Map()
  }

  static addNode (graph, nodeToAdd) {
    if (!graph.nodeList.has(nodeToAdd)) {
      graph.nodeList.set(nodeToAdd, false)
    }
  }

  static deleteNode (graph, nodeToDelete) {
    if (graph.nodeList.has(nodeToDelete)) {
      // 인접리스트에 없는 항목이어야 삭제할 수 있다.
      if (graph.adjacencyList.get(nodeToDelete) && graph.adjacencyList.get(nodeToDelete).length) return
      graph.nodeList.delete(nodeToDelete)
    }
  }

  // 간선의 중첩은 다루지 않음 만약 다룬다면 오브젝트로 다뤄야함.
  static addEdge (graph, preNode, postNode, isOneWay) {
    if (!graph.adjacencyList.has(preNode)) {
      graph.adjacencyList.set(preNode, [postNode])
    } else {
      graph.adjacencyList.get(preNode).push(postNode)
    }
    if (!isOneWay) {
      if (!graph.adjacencyList.has(postNode)) {
        graph.adjacencyList.set(postNode, [preNode])
      } else {
        graph.adjacencyList.get(postNode).push(preNode)
      }
    }
  }

  static deleteEdge (graph, preNode, postNode, isOneWay) {
    if (!graph.adjacencyList.has(preNode) || !graph.adjacencyList.has(postNode)) return
    const index = graph.adjacencyList.get(preNode).indexOf(postNode)
    graph.adjacencyList.get(preNode).splice(index, 1)
    if (!isOneWay) {
      const index = graph.adjacencyList.get(postNode).indexOf(postNode)
      graph.adjacencyList.get(postNode).splice(index, 1)
    }
  }
}

const g = new Graph()
Graph.addNode(g, 'A')
Graph.addNode(g, 'X')
Graph.addNode(g, 'G')
Graph.addNode(g, 'H')
Graph.addNode(g, 'P')
Graph.addNode(g, 'E')
Graph.addNode(g, 'M')
Graph.addNode(g, 'Y')
Graph.addNode(g, 'J')
Graph.addEdge(g, 'A', 'X')
Graph.addEdge(g, 'X', 'G')
Graph.addEdge(g, 'X', 'H')
Graph.addEdge(g, 'G', 'H')
Graph.addEdge(g, 'G', 'P')
Graph.addEdge(g, 'H', 'E')
Graph.addEdge(g, 'H', 'P')
Graph.addEdge(g, 'E', 'M')
Graph.addEdge(g, 'E', 'Y')
Graph.addEdge(g, 'Y', 'M')
Graph.addEdge(g, 'M', 'J')
console.log(g.nodeList)
console.log(g.adjacencyList)


//   ```
// A-X-G-P
//   | |/
//   |_H_E_M_J
//       |/
//       Y
// ```
let result = []
let start = 'A'
let queue = []
let visited = new Map()
//start는 시작점
queue.push(start)
visited.set(start, true)
while (queue.length) {
  const first = queue[0]
  result.push(queue.shift())
  //adjacencyList는 인접리스트로 이미 만들었다 가정함
  if (g.adjacencyList.get(first) && g.adjacencyList.get(first).length) {
    g.adjacencyList.get(first).forEach(node => {
      if (!visited.get(node)) {
        queue.push(node)
        visited.set(node, true)
      }
    })
  }
}
console.log(result.join(' '))
