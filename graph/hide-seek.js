/**
숨바꼭질 출처다국어분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
2 초	128 MB	88057	24331	15145	24.821%
문제
수빈이는 동생과 숨바꼭질을 하고 있다. 수빈이는 현재 점 N(0 ≤ N ≤ 100,000)에 있고, 동생은 점 K(0 ≤ K ≤ 100,000)에 있다. 수빈이는 걷거나 순간이동을 할 수 있다. 만약, 수빈이의 위치가 X일 때 걷는다면 1초 후에 X-1 또는 X+1로 이동하게 된다. 순간이동을 하는 경우에는 1초 후에 2*X의 위치로 이동하게 된다.

수빈이와 동생의 위치가 주어졌을 때, 수빈이가 동생을 찾을 수 있는 가장 빠른 시간이 몇 초 후인지 구하는 프로그램을 작성하시오.

입력
첫 번째 줄에 수빈이가 있는 위치 N과 동생이 있는 위치 K가 주어진다. N과 K는 정수이다.

출력
수빈이가 동생을 찾는 가장 빠른 시간을 출력한다.

예제 입력 1
5 17
예제 출력 1
4
*/
const N = 0
const K = 100000
// let input = require('fs').readFileSync('/dev/stdin')
// let data = input.toString().trim().split(/\s+/).map(i => +i)
// const [N, K] = data
const visited = new Map()
const isBigger = N > K
let time = 0
const bfs = (start, time) => {
  const queue = []
  queue.push([start, time])
  visited.set(start, true)
  while (queue.length) {
    const [first, time] = queue[0]
    queue.shift()
    let next = null
    let sec = null
    for (let i = 0; i < 3; i++) {
      if (i === 0) {
        next = first + 1
        sec = time + 1
      } else if (i === 1) {
        next = first - 1
        sec = time + 1
      } else if (i === 2) {
        next = first * 2
        sec = time + 1
      }
      if (next === K) {
        console.log(sec)
        return
      }
      if (!visited.get(next) && next >= 0 && next <= 100000) {
        queue.push([next, sec])
        visited.set(next, true)
      }
    }
  }
}
if (N === K) {
  console.log(time)
} else if (isBigger) {
  let X = N
  while (X !== K) {
    X--
    time++
  }
  console.log(time)
} else {
  bfs(N,0)
}
