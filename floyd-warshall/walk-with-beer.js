/**
맥주 마시면서 걸어가기 출처다국어분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
1 초	128 MB	9573	3365	2509	35.209%
문제
송도에 사는 상근이와 친구들은 송도에서 열리는 펜타포트 락 페스티벌에 가려고 한다. 올해는 맥주를 마시면서 걸어가기로 했다. 출발은 상근이네 집에서 하고, 맥주 한 박스를 들고 출발한다. 맥주 한 박스에는 맥주가 20개 들어있다. 목이 마르면 안되기 때문에 50미터에 한 병씩 마시려고 한다.

상근이의 집에서 페스티벌이 열리는 곳은 매우 먼 거리이다. 따라서, 맥주를 더 구매해야 할 수도 있다. 미리 인터넷으로 조사를 해보니 다행히도 맥주를 파는 편의점이 있다. 편의점에 들렸을 때, 빈 병은 버리고 새 맥주 병을 살 수 있다. 하지만, 박스에 들어있는 맥주는 20병을 넘을 수 없다.

편의점, 상근이네 집, 펜타포트 락 페스티벌의 좌표가 주어진다. 상근이와 친구들이 행복하게 페스티벌에 도착할 수 있는지 구하는 프로그램을 작성하시오.

입력
첫째 줄에 테스트 케이스의 개수 t가 주어진다. (t ≤ 50)

각 테스트 케이스의 첫째 줄에는 맥주를 파는 편의점의 개수 n이 주어진다. (0 ≤ n ≤ 100).

다음 n+2개 줄에는 상근이네 집, 편의점, 펜타포트 락 페스티벌 좌표가 주어진다. 각 좌표는 두 정수 x와 y로 이루어져 있다. (두 값 모두 미터, -32768 ≤ x, y ≤ 32767)

송도는 직사각형 모양으로 생긴 도시이다. 두 좌표 사이의 거리는 x 좌표의 차이 + y 좌표의 차이 이다. (맨해튼 거리)

출력
각 테스트 케이스에 대해서 상근이와 친구들이 행복하게 페스티벌에 갈 수 있으면 "happy", 중간에 맥주가 바닥나면 "sad"를 출력한다.
*/

// const data = [
//   [2],
//   [2],
//   [0, 0],
//   [1000, 0],
//   [1000, 1000],
//   [2000, 1000],
//   [2],
//   [0, 0],
//   [1000, 0],
//   [2000, 1000],
//   [2000, 2000]
// ]
let input = require('fs').readFileSync('/dev/stdin')
let data = input.toString().trim().split(/\n+/).map(i => i.toString().trim().split(/\s+/).map(n => +n))

const setInfos = (data) => {
  let n = null
  const xys = []
  const info = []
  for (let i = 1; i < data.length; i++) {
    if (data[i].length === 1) {
      n = data[i][0]
    } else {
      xys.push(data[i])
    }
    if (xys.length === n + 2) {
      info.push({
        n,
        xy: [...xys]
      })
      n = null
      xys.length = 0
    }
  }
  return info
}

class Graph {
  constructor() {
    this.nodeList = new Map()
    this.adjList = new Map()
  }

  addNode(node, value) {
    if (!this.nodeList.has(node)) {
      this.nodeList.set(node, value)
    }
  }

  addEdge(pre, post) {
    if (!this.adjList.has(pre)) {
      this.adjList.set(pre, [post])
    } else {
      this.adjList.get(pre).push(post)
    }
  }
}

const bfs = (graph, start) => {
  const queue = []
  const visited = new Map()
  queue.push(start)
  visited.set(start, true)
  while (queue.length) {
    const first = queue[0]
    queue.shift()
    if (graph.adjList.get(first) && graph.adjList.get(first).length) {
      graph.adjList.get(first).forEach(node => {
        if (!visited.get(node)) {
          const [x1, y1] = graph.nodeList.get(first)
          const [x2, y2] = graph.nodeList.get(node)
          const distance = Math.abs(x1 - x2) + Math.abs(y1 - y2)
          if (distance <= 1000) {
            queue.push(node)
            visited.set(node, true)
          }
        }
      })
    }
  }
  console.log(visited.get(graph.nodeList.size - 1) ? 'happy' : 'sad')
}

const setNode = (nodes) => {
  const graph = new Graph()
  let start = 0
  nodes.forEach((node, index) => {
    graph.addNode(index, node)
    while (start <= nodes.length - 1) {
      if (start !== index) {
        graph.addEdge(start, index)
      }
      start++
    }
    start = 0
  })
  return graph
}

const main = () => {
  const t = data[0]
  const infos = setInfos(data)
  for (let i = 0; i < t; i++) {
    const graph = setNode(infos[i].xy)
    bfs(graph, 0)
  }
}

main()
