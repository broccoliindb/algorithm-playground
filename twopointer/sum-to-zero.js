/**
Lower Bound와 Upper Bound는 일종의 이분 탐색에서 파생된 것으로,

이분 탐색이 '원하는 값 k를 찾는 과정' 이라면 Lower Bound는 '원하는 값 k 이상이 처음 나오는 위치를 찾는 과정' 이며, Upper Bound는 '원하는 값 k를 초과한 값이 처음 나오는 위치를 찾는 과정'이다.
*/


/**
문제
정수로 이루어진 크기가 같은 배열 A, B, C, D가 있다.

A[a], B[b], C[c], D[d]의 합이 0인 (a, b, c, d) 쌍의 개수를 구하는 프로그램을 작성하시오.

입력
첫째 줄에 배열의 크기 n (1 ≤ n ≤ 4000)이 주어진다. 다음 n개 줄에는 A, B, C, D에 포함되는 정수가 공백으로 구분되어져서 주어진다. 배열에 들어있는 정수의 절댓값은 최대 228이다.

출력
합이 0이 되는 쌍의 개수를 출력한다.

예제 입력 1
6
-45 22 42 -16
-41 -27 56 30
-36 53 -37 77
-36 30 -75 -46
26 -38 -10 62
-32 -54 -6 45

-45 22 42 -16 -41 -27 56 30 -36 53 -37 77 -36 30 -75 -46 26 -38 -10 62 -32 -54 -6 45
예제 출력 1
5
*/


function getResult(subSum) {
  let rst = {}
  let N = +subSum[0]
  i = N
  while (i >= 1) {
    let j = N
    while (j >= 1) {
      const sum = subSum[i][2] + subSum[j][3]
      rst[sum] ? ++rst[sum] : rst[sum] = 1
      j--
    }
    i--
  }

  i = N
  let count = 0
  while (i >= 1) {
    let j = N
    while (j >= 1) {
      const sum = - (subSum[i][0] + subSum[j][1])
      if (rst[sum]) count += rst[sum]
      j--
    }
    i--
  }

  console.log(count)
}

let input = require("fs").readFileSync('/dev/stdin');

const s = new Date()
getResult(input.toString().trim().split(/\n+/).map(a => a.trim().split(/\s+/).map(n => +n)))
const e = new Date()
console.log(e - s)


