// let input = require("fs").readFileSync('/dev/stdin').toString().trim().split('\n');
// const N = parseInt(input[0].split(' ')[0], 10)
// const S = parseInt(input[0].split(' ')[1], 10)
// const Arr = input[1].split(' ').map(i => parseInt(i, 10))

const readline = require('readline')
const r = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const getResult = (N, S, Arr) => {
  let start = 0
  let end = 0
  let sum = Arr[end]
  let result = 0
  let count = 0
  while (start <= end && end < Arr.length) {
    const length = end - start + 1
    console.log(start, end, length, S)
    if (sum === S) {
      if (result === 0) {
        result = length
      } else {
        result = result <= length ? result : length
      }
      end++
      sum += Arr[end]
    } else if (sum < S) {
      end++
      sum += Arr[end]
    } else {
      if (result === 0) {
        result = length
      } else {
        result = result <= length ? result : length
      }
      sum -= Arr[start]
      start++
    }
  }
  console.log(result)
}

let input = []

r.on('line', line => {
  input.push(line.trim())
  if (input.length === 2) {
    const N = parseInt(input[0].split(' ')[0], 10)
    const S = parseInt(input[0].split(' ')[1], 10)
    const Arr = input[1].split(' ').map(i => parseInt(i, 10))
    getResult(N, S, Arr)
    input.length = 0
  }
})
  .on('close', () => { process.exit() })