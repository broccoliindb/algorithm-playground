const readline = require('readline')
const r = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

let input = []
r.on('line', (line) => {
  input.push(line)
  if (input.length === 2) {
    const N = parseInt(input[0].split(' ')[0], 10)
    const M = parseInt(input[0].split(' ')[1], 10)
    const Arr = input[1].split(' ').map(i => parseInt(i, 10))

    let start = 0
    let end = 0
    let sum = Arr[end]
    let count = 0
    while (end < Arr.length) {
      if (sum === M) {
        // console.log(start, end, sum, M)
        count++
        end++
        sum = sum + Arr[end] - Arr[start]
        start++
      } else if (sum < M) {
        // console.log(start, end, sum, M)
        end++
        sum += Arr[end]
      } else {
        // console.log(start, end, sum, M)
        sum -= Arr[start]
        start++
      }
    }
    console.log(count)
    input.length = 0
  }

}).on('close', () => {

})