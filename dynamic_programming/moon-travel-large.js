/**
진우의 달 여행 (Large) 출처분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
1 초	256 MB	492	212	156	41.600%
문제
우주비행이 꿈이였던 진우는 음식점 '매일매일싱싱'에서 열심히 일한 결과 달 여행에 필요한 자금을 모두 마련하였다! 지구와 우주사이는 N X M 행렬로 나타낼 수 있으며 각 원소의 값은 우주선이 그 공간을 지날 때 소모되는 연료의 양이다.

[예시]



진우는 여행경비를 아끼기 위해 조금 특이한 우주선을 선택하였다. 진우가 선택한 우주선의 특징은 아래와 같다.

1. 지구 -> 달로 가는 경우 우주선이 움직일 수 있는 방향은 아래와 같다.



2. 우주선은 전에 움직인 방향으로 움직일 수 없다. 즉, 같은 방향으로 두번 연속으로 움직일 수 없다.

진우의 목표는 연료를 최대한 아끼며 지구의 어느위치에서든 출발하여 달의 어느위치든 착륙하는 것이다.

최대한 돈을 아끼고 살아서 달에 도착하고 싶은 진우를 위해 달에 도달하기 위해 필요한 연료의 최소값을 계산해 주자.

입력
첫줄에 지구와 달 사이 공간을 나타내는 행렬의 크기를 나타내는 N, M (2 ≤ N, M ≤ 1000)이 주어진다.

다음 N줄 동안 각 행렬의 원소 값이 주어진다. 각 행렬의 원소값은 100 이하의 자연수이다.

출력
달 여행에 필요한 최소 연료의 값을 출력한다.

예제 입력 1 
6 4
5 8 5 1
3 5 8 4
9 77 65 5
2 1 5 2
5 98 1 5
4 95 67 58
예제 출력 1 
29
*/
// const N = 6
// const M = 4
// const data = [
//   [5, 8, 5, 1],
//   [3, 5, 8, 4],
//   [9, 77, 65, 5],
//   [2, 1, 5, 2],
//   [5, 98, 1, 5],
//   [4, 95, 67, 58]
// ]

/**
 * 0: 아래방향
 * 1: 우아래방향
 * 2: 좌아래방향
 */

const input = require('fs').readFileSync('/dev/stdin')
const info = input
  .toString()
  .trim()
  .split(/\n+/)
  .map((i) =>
    i
      .toString()
      .trim()
      .split(/\s+/)
      .map((n) => +n)
  )
const [N, M] = info[0]
const data = info.slice(1)
const dp = Array.from({ length: N }).map((i) =>
  Array.from({ length: M }).map((j) => Array.from({ length: 3 }))
)

for (let x = 0; x < M; x++) {
  for (let k = 0; k < 3; k++) {
    dp[0][x][k] = data[0][x]
  }
}
for (let y = 1; y < N; y++) {
  for (let x = 0; x < M; x++) {
    //좌측 끝
    if (x === 0) {
      dp[y][x][0] = dp[y - 1][x + 1][2] + data[y][x]
      dp[y][x][1] = Math.min(dp[y - 1][x][0], dp[y - 1][x + 1][2]) + data[y][x]
      //우측끝
    } else if (x === M - 1) {
      dp[y][x][0] = dp[y - 1][x - 1][1] + data[y][x]
      dp[y][x][2] = Math.min(dp[y - 1][x][0], dp[y - 1][x - 1][1]) + data[y][x]
      //사이
    } else {
      dp[y][x][0] =
        Math.min(dp[y - 1][x - 1][1], dp[y - 1][x + 1][2]) + data[y][x]
      dp[y][x][1] = Math.min(dp[y - 1][x][0], dp[y - 1][x + 1][2]) + data[y][x]
      dp[y][x][2] = Math.min(dp[y - 1][x][0], dp[y - 1][x - 1][1]) + data[y][x]
    }
  }
}
let min = Infinity
for (let x = 0; x < M; x++) {
  for (let k = 0; k < 3; k++) {
    if (!dp[N - 1][x][k]) continue
    if (dp[N - 1][x][k] < min) {
      min = dp[N - 1][x][k]
    }
  }
}
console.log(min)
