/**
1로 만들기 분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
2 초	128 MB	124057	41468	25902	32.210%
문제
정수 X에 사용할 수 있는 연산은 다음과 같이 세 가지 이다.

X가 3으로 나누어 떨어지면, 3으로 나눈다.
X가 2로 나누어 떨어지면, 2로 나눈다.
1을 뺀다.
정수 N이 주어졌을 때, 위와 같은 연산 세 개를 적절히 사용해서 1을 만들려고 한다. 연산을 사용하는 횟수의 최솟값을 출력하시오.

입력
첫째 줄에 1보다 크거나 같고, 10^6보다 작거나 같은 정수 N이 주어진다.

출력
첫째 줄에 연산을 하는 횟수의 최솟값을 출력한다.

예제 입력 1
2
예제 출력 1
1
예제 입력 2
10
예제 출력 2
3
*/
const N = parseInt(require('fs').readFileSync('/dev/stdin').toString().trim(), 10)
const bfs = (N) => {
  let count = 0
  let queue = []
  const visited = new Set()
  queue.push(N)
  visited.add(N)
  while (queue.length) {
    const newQueue = []
    for (const value of queue) {
      if (value === 1) {
        console.log(count)
        return
      }
      if (value % 3 === 0 && !visited.has(value / 3)) {
        const temp = Math.floor(value / 3)
        newQueue.push(temp)
        visited.add(temp)
      }
      if (value % 2 === 0 && !visited.has(value / 2)) {
        const temp = Math.floor(value / 2)
        newQueue.push(temp)
        visited.add(temp)
      }
      {
        const temp = value - 1
        if (!visited.has(temp)) {
          newQueue.push(temp)
          visited.add(temp)
        }
      }
    }
    queue = newQueue
    count++
  }
}
bfs(N)