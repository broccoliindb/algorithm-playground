/**
2×n 타일링 분류
시간 제한	메모리 제한	제출	정답	맞은 사람	정답 비율
1 초	256 MB	68217	25164	18458	34.646%
문제
2×n 크기의 직사각형을 1×2, 2×1 타일로 채우는 방법의 수를 구하는 프로그램을 작성하시오.

아래 그림은 2×5 크기의 직사각형을 채운 한 가지 방법의 예이다.



입력
첫째 줄에 n이 주어진다. (1 ≤ n ≤ 1,000)

출력
첫째 줄에 2×n 크기의 직사각형을 채우는 방법의 수를 10,007로 나눈 나머지를 출력한다.

예제 입력 1 
2
예제 출력 1 
2
예제 입력 2 
9
예제 출력 2 
55
*/
let N = parseInt(
  require("fs").readFileSync("/dev/stdin").toString().trim(),
  10
);
// let N = 9;
const T = (N, save) => {
  if (N === 1) {
    save[N] = 1;
    return save[N];
  }
  if (N === 2) {
    save[N] = 2;
    return save[N];
  }
  if (N === 3) {
    save[N] = 3;
    return save[N];
  }
  if (N >= 4) {
    const tempN_1 = save[N - 1] || T(N - 1, save);
    const tempN_2 = save[N - 2] || T(N - 2, save);
    save[N] = (tempN_1 + tempN_2) % 10007;
    return save[N];
  }
};

const getResult = (N) => {
  const save = {};
  return T(N, save);
};
console.log(getResult(N));
